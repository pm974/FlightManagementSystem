﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;

namespace FlightManagementSystem.Converters
{
    public class BoolToVisibilityOppositeConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            bool inputValue = (bool)value;

            if (inputValue)
                return Visibility.Collapsed;
            else if (parameter != null && parameter.ToString() == "Hidden" && inputValue)
                return Visibility.Hidden;
            else
                return Visibility.Visible;             
        }

        public object ConvertBack(object value, Type targetType,object parameter, CultureInfo culture)
        {
            return null;
        }
    }
}