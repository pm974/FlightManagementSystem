﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;

namespace FlightManagementSystem.Converters
{
    public class NullOrEmptyToVisibilityConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            string inputValue = (string)value;
            string inputParameter = (string)value;

            if (!string.IsNullOrEmpty(inputValue))
                return Visibility.Visible;
            else if (inputParameter != null && inputParameter == "Hidden" && string.IsNullOrEmpty(inputValue))
                return Visibility.Hidden;
            else
                return Visibility.Collapsed;           
        }

        public object ConvertBack(object value, Type targetType,object parameter, CultureInfo culture)
        {
            return null;
        }
    }
}