﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace FlightManagementSystem.Model
{
    public class MenuButton
    {
        public string ButtonText { get; set; }
        public ICommand ButtonCommand { get; set; }
        public int PermissionLevel { get; set; }
    }
}
