﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace FlightManagementSystem.Model
{
    public class Notification
    {
        public string NotificationText { get; set; }
        public ICommand DismissNotification { get; set; }
        public bool Urgent { get; set; }
    }
}
