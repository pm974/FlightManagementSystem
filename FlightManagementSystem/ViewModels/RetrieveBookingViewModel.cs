﻿using FlightManagementSystem.ViewModels.BaseViewModels;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace FlightManagementSystem.ViewModels
{
    public class RetrieveBookingViewModel : ViewModelBase
    {
        private string bookingID;
        public string BookingID
        {
            get { return bookingID; }
            set { bookingID = value; OnPropertyChanged("BookingID"); }
        }

        private string firstName;
        public string FirstName
        {
            get { return firstName; }
            set { firstName = value; OnPropertyChanged("FirstName"); }
        }

        private string lastName;
        public string LastName
        {
            get { return lastName; }
            set { lastName = value; OnPropertyChanged("LastName"); }
        }

        private ICommand getBookingDetails;
        public ICommand GetBookingDetails
        {
            get { return getBookingDetails = getBookingDetails ?? new RelayCommand(ExecuteGetBookingDetailsCommand); }
        }

        MySqlConnection connection;
        MySqlCommand command;
        MySqlDataReader reader;

        private void ExecuteGetBookingDetailsCommand(object obj)
        {
            string query = "SELECT login_meta_details_username, login_meta_details_password FROM login_meta_details WHERE (login_meta_details_id = " + BookingID + ")";
            String connString = "Database=fmsdatabase;Data Source=au-cdbr-azure-southeast-a.cloudapp.net;User Id=bcb629dda94915;Password=35142dc6";
            connection = new MySqlConnection(connString);
            connection.Open();
            command = new MySqlCommand(query, connection);
            reader = command.ExecuteReader();

            while (reader.Read())
            {
                FirstName = reader.GetString(0);
                LastName = reader.GetString(1);
            }

            if (reader != null)
            {
                reader.Close();
            }
            if (connection != null)
            {
                connection.Close(); //close the connection
            }
        }
    }
}
