﻿using FlightManagementSystem.ViewModels.BaseViewModels;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace FlightManagementSystem.ViewModels
{
    public class SignUpViewModel : ViewModelBase
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string DateOfBirth { get; set; }
        public string Address { get; set; }
        public string PhoneNumber { get; set; }
        public string PassportID { get; set; }
        public string EmailAddress { get; set; }
        public string UserUUID { get; set; }
        public int PermissionLevel { get; set; }
        public string Password { get; set; }
        public string ConfirmPassword { get; set; }


        private ICommand submitAndCreateAccountCommand;
        public ICommand SubmitAndCreateAccountCommand
        {
            get { return submitAndCreateAccountCommand = submitAndCreateAccountCommand ?? new RelayCommand(ExecuteSubmitAndCreateAccountCommand); }
        }

        public SignUpViewModel()
        {
            UserUUID = Guid.NewGuid().ToString();
            PermissionLevel = 0;
            OnPropertyChanged("UserUUID");
        }

        private void ExecuteSubmitAndCreateAccountCommand(object obj)
        {
            String connectionString = "Database=fmsdatabase;Data Source=au-cdbr-azure-southeast-a.cloudapp.net;User Id=bcb629dda94915;Password=35142dc6";
            MySqlConnection connection = new MySqlConnection(connectionString);

            MySqlCommand command = new MySqlCommand("INSERT INTO fmsdatabase.user_account_details (uuid, first_name, last_name, dob, address, phonenumber, passportid, emailaddress, password, permissionlevel)VALUES(@UserUUID, @FirstName, @LastName, @DateOfBirth, @Address, @PhoneNumber, @PassportID, @EmailAddress, @Password, @PermissionLevel)");
            command.CommandType = CommandType.Text;
            command.Connection = connection;
            command.Parameters.AddWithValue("@UserUUID", UserUUID);
            command.Parameters.AddWithValue("@FirstName", FirstName);
            command.Parameters.AddWithValue("@LastName", LastName);
            command.Parameters.AddWithValue("@DateOfBirth", DateOfBirth);
            command.Parameters.AddWithValue("@Address", Address);
            command.Parameters.AddWithValue("@PhoneNumber", PhoneNumber);
            command.Parameters.AddWithValue("@PassportID", PassportID);
            command.Parameters.AddWithValue("@EmailAddress", EmailAddress);
            command.Parameters.AddWithValue("@Password", Password);
            command.Parameters.AddWithValue("@PermissionLevel", PermissionLevel);

            connection.Open();
            command.ExecuteNonQuery();

            connection.Close();
        }
    }
}
